﻿using UnityEngine;
using System.Collections;

public class DestroyNME : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision col){

		if (col.gameObject.tag == "Obs") {
		
			Destroy (col.gameObject);
			Destroy (gameObject);
		
		}
	
	}
		
}
