﻿using UnityEngine;
using System.Collections;

public class SpawnManager : MonoBehaviour {
	public GameObject[] SpawnPoints;
	public GameObject Obstacles;
	// Use this for initialization
	void Start () {
		InvokeRepeating ("ChooseBullet", 3.0f, 1f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}



	void ChooseBullet(){
	
		int NextSpawn = Random.Range (0, 4);
		SpawnBullet (SpawnPoints [NextSpawn]);

	}

	void SpawnBullet(GameObject chosenSpawn){
	
		Instantiate (Obstacles, new Vector3 (chosenSpawn.transform.position.x, chosenSpawn.transform.position.y, chosenSpawn.transform.position.z), Quaternion.identity);
	
	}
}
