﻿using UnityEngine;
using System.Collections;

public class Shooooooot : MonoBehaviour {
	Rigidbody rb;
	public float bulletSpeed;
	// Use this for initialization
	void Start () {

		rb = gameObject.GetComponent<Rigidbody> ();
		rb.velocity = Vector3.right * bulletSpeed;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
