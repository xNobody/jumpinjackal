﻿using UnityEngine;
using System.Collections;

public class Autojumpo : MonoBehaviour {
	public Rigidbody rb;
	public float autoJumpHeight;
	public float smashJumpHeight;
	bool isSmashing;
	bool onFloor;
	bool smashJump;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space) == true) {
			
			isSmashing = true;

						
		}
		if (Input.GetKeyUp (KeyCode.Space) == true) {
		
			isSmashing = false;
			smashJump = false;
		
		}

		if (isSmashing) {
			rb.velocity -= 2 * autoJumpHeight * Vector3.up;
			smashJump = true;
			isSmashing = false;

		}

		if (onFloor && !isSmashing && !smashJump) {
			Jump (autoJumpHeight);
			onFloor = false;
		}

		if (smashJump && onFloor) {
		
			Jump (smashJumpHeight);
		
		}

	}




	void Jump(float jumpHeight){
	
		rb.velocity += jumpHeight * Vector3.up;
	
	}




	void SmashDown(){
	

		if (onFloor) {
			
			Jump (smashJumpHeight);
		}



	}




	void OnCollisionEnter(Collision col){
		
		if (col.gameObject.name == "Floor") {
			onFloor = true;
					
		}
	
	}

	void OnCollisionExit(Collision col){
		if (col.gameObject.name == "Floor") {
			onFloor = false;
		
		}
	
	}

}
